public class Player {
    private String name;
    private char playRole;

    public Player(String name, char playRole) {
        this.name = name;
        this.playRole = playRole;
    }

    public String getName() {
        return name;
    }

    public char getPlayRole() {
        return playRole;
    }
}
