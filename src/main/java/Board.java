import java.util.Arrays;

public class Board {
    private final char[][] board;

    public Board(int n) {
        this.board = new char[n][n];
    }

    Board(char[][] boardTab) {
        this.board = boardTab;
    }

    public void initBoard(int n) {
        for (int i = 0; i < n; i++) {
            char[] horizontal = new char[n];
            Arrays.fill(horizontal, '_');
            board[i] = horizontal;
        }
    }

    public void showBoard() {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }
    }

    public boolean setSymbol(int x, int y, char symbol) {
        if (x >= board.length || y >= board.length) {
            System.out.println("Podano współrzędne znajdujące się poza planszą");
            return false;
        } else if (board[x][y] != '_') {
            System.out.println("Nie możesz wybrać pola już zajętego");
            return false;
        }
        board[x][y] = symbol;
        return true;
    }

    boolean isDraw() {
        int p = 0;
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                if (board[i][j] == '_') {
                    p++;
                }
            }
        }
        if (p == 0) {
            System.out.println("Remis, proszę Państwa =)");
            return true;
        } else return false;
    }

    boolean checkHorizontals() {

        for (int i = 0; i < board.length; i++) {
            char symbol = board[i][0];
            int p = 1;
            for (int j = 1; j < board.length; j++) {
                char currantSymbol = board[i][j];
                if (symbol == currantSymbol && symbol != '_') {
                    p++;
                }
            }
            if (p == board.length) {
                return true;
            }
        }
        return false;
    }


    boolean checkVerticals() {
        for (int i = 0; i < board.length; i++) {
            char symbol = board[0][i];
            int p = 1;
            for (int j = 1; j < board.length; j++) {
                char currantSymbol = board[j][i];
                if (symbol == currantSymbol && symbol != '_') {
                    p++;
                }
            }
            if (p == board.length) {
                return true;
            }
        }
        return false;
    }

    boolean checkDiagonals1() {
        char symbol = board[0][0];
        for (int i = 1; i < board.length; i++) {
            char currantSymbol = board[i][i];
            if (currantSymbol != symbol) {
                return false;
            }
            if (symbol == "_".charAt(0)) {
                return false;
            }
        }
        System.out.println("Zwycięstwo!!!");
        return true;
    }

    boolean checkDiagonals2() {
        char symbol1 = board[0][board.length - 1];
        for (int i = 1; i < board.length; i++) {
            char currantSymbol = board[i][board.length - i - 1];
            if (currantSymbol != symbol1) {
                return false;
            }
            if (symbol1 == '_') {
                return false;
            }
        }
        System.out.println("Zwycięstwo!!!");
        return true;
    }

    boolean isGameFinished() {
        return checkDiagonals1() || checkDiagonals2() || checkHorizontals() || checkVerticals();
    }

}
