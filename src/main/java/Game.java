import java.util.Scanner;

public class Game {
    private Board board;
    private Player firstPlayer;
    private Player secondPlayer;
    private int boardSize;
    private Scanner scanner;
    private Player activePlayer;
    private int [] coordinate;
    private void initGame() {
        coordinate = new int[2];
        System.out.println("Gra kółko i krzyżyk");

        scanner = new Scanner(System.in);
        System.out.println("Imię gracza - kółko:");
        String playerName = scanner.nextLine();
        firstPlayer = new Player(playerName, "O".charAt(0));

        activePlayer = firstPlayer;

        System.out.println("Imię gracza - krzyżyk:");
        playerName = scanner.nextLine();
        secondPlayer = new Player(playerName, "X".charAt(0));

        System.out.println("Podaj wymiar planszy:");
        boardSize = scanner.nextInt();

        board = new Board(boardSize);
        board.initBoard(boardSize);
    }

    public void startGame() {
        initGame();
        while (!board.isGameFinished() && !board.isDraw()) {
            board.showBoard();
            System.out.println("Ruch gracza: " + activePlayer.getName());
            getCoordinates();
            while (!board.setSymbol(coordinate[0],coordinate[1],activePlayer.getPlayRole())){
                getCoordinates();
            }
            switchActivePlayer();
        }
        switchActivePlayer();
        System.out.println("Wygrał: "+activePlayer.getName());
        board.showBoard();
    }

     void getCoordinates() {
        System.out.println("Podaj współrzędną pionowa");
        coordinate[0] = scanner.nextInt();
        System.out.println("Podaj współrzędną pozioma");
        coordinate[1] = scanner.nextInt();
    }

    private void switchActivePlayer(){
        if(activePlayer.equals(firstPlayer)){
            activePlayer = secondPlayer;
        } else {
            activePlayer = firstPlayer;
        }
    }

}
