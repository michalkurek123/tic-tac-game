import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class PlayerTest {
    @Test
    void TestPlayer() {
        //given
        Player player = new Player("mk", 'X');
        //when
        //then
        assertNotNull(player);
        assertEquals("mk",player.getName());
        assertEquals('X', player.getPlayRole());
    }
}
