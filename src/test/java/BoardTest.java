import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BoardTest {

    @Test
    void TestBoardDraw() {
        //given
        char[][] boardTab1 = new char[][]{{'O', 'X', 'O'}, {'O', 'X', 'X'}, {'X', 'O', 'O'}};
        char[][] boardTab2 = new char[][]{{'O', '_', 'O'}, {'O', 'X', 'X'}, {'X', 'X', 'O'}};
        //when
        //then
        assertEquals(true, new Board(boardTab1).isDraw());
        assertEquals(false, new Board(boardTab2).isDraw());
    }

    @Test
    void TestBoardVertical() {
        //given
        char[][] boardTab1 = new char[][]{{'O', 'X', 'O'}, {'X', 'O', 'O'}, {'X', 'O', 'O'}};
        char[][] boardTab2 = new char[][]{{'O', '_', 'O'}, {'O', 'O', 'X'}, {'X', 'X', 'X'}};
        //when
        //then
        assertEquals(true, new Board(boardTab1).checkVerticals());
        assertEquals(false, new Board(boardTab2).checkVerticals());
    }

    @Test
    void TestBoardHorizontal() {
        //given
        char[][] boardTab1 = new char[][]{{'O', 'X', 'O'}, {'X', 'X', 'X'}, {'X', 'O', 'O'}};
        char[][] boardTab2 = new char[][]{{'O', '_', 'X'}, {'O', 'O', 'X'}, {'X', 'O', 'X'}};
        //when
        //then
        assertEquals(true, new Board(boardTab1).checkHorizontals());
        assertEquals(false, new Board(boardTab2).checkHorizontals());
    }

    @Test
    void TestBoardDiagonal1() {
        //given
        char[][] boardTab1 = new char[][]{{'O', 'X', 'O'}, {'X', 'O', 'X'}, {'X', 'O', 'O'}};
        char[][] boardTab2 = new char[][]{{'X', '_', 'O'}, {'O', '0', 'X'}, {'0', 'O', 'X'}};
        //when
        //then
        assertEquals(true, new Board(boardTab1).checkDiagonals1());
        assertEquals(false, new Board(boardTab2).checkDiagonals1());
    }

    @Test
    void TestBoardDiagonal2() {
        //given
        char[][] boardTab1 = new char[][]{{'O', 'X', 'O'}, {'X', 'O', 'X'}, {'X', 'O', 'O'}};
        char[][] boardTab2 = new char[][]{{'X', '_', 'O'}, {'O', 'O', 'X'}, {'O', 'O', 'X'}};
        //when
        //then
        assertEquals(false, new Board(boardTab1).checkDiagonals2());
        assertEquals(true, new Board(boardTab2).checkDiagonals2());
    }

}
